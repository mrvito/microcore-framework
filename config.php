<?php

return array(

    /*
     * Main application settings
     */
    'app' => array(
        'debug' => false,
        'domain' => '//domain.com',
    ),

    /**
     * Various default application paths
     */
    'paths' => array(
        'views' => 'app/views/',
        'assets' => 'public/',
        'controllers' => 'app/controllers',
        'models' => 'app/models',
        'library' => 'app/library'
    ),

    /*
     * Database settings for MySQL database
     */
    'database' => array(
        'connect' => true,
        'host' => 'localhost',
        'database' => 'microcore',
        'user' => 'microcore',
        'password' => 'microcore'
    ),

    /**
     * Various miscellaneous settings
     */
    'misc' => array(
        'whoops-view' => FRAMEWORK_ROOT . '/views/whoops.php',
        'error-view' => FRAMEWORK_ROOT . '/views/error.php',
    )
);