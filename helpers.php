<?php

/**
 * Return a value from a multidimensional array using a dot notated path.
 * @param array $array
 * @param string $path
 * @param mixed $default
 * @return mixed|null
 */
function array_dot($array, $path, $default = null)
{
    $current = $array;
    $pathToken = strtok($path, '.');

    while ($pathToken !== false) {

        if (!isset($current[$pathToken])) {
            return $default;
        }

        $current = $current[$pathToken];
        $pathToken = strtok('.');
    }

    return $current;
}

function str_ends_with($needle, $string)
{
    return substr($string, strlen($string) - 1) == $needle;
}

/**
 * Get a full URL to a local path
 * @param string $path Path to a file
 * @return string
 */
function asset($path)
{
    return '/' . Config::get('paths.assets') . $path;
}

/**
 * A shortcut function for Assets::insertScripts
 * @return string
 */
function scripts()
{
    return Assets::insertScripts();
}

/**
 * A shortcut function for Assets::insertStyles
 * @return string
 */
function styles()
{
    return Assets::insertStyles();
}

/**
 * A shortcut function for Config::get
 * @param $path
 * @return mixed|null
 */
function conf($path)
{
    return Config::get($path);
}

/**
 * A shortcut function for View::extend
 * @param $layout
 */
function extend($layout)
{
    View::extend($layout);
}

/**
 * A shortcut function for View::place
 * @param $section
 * @return string|null
 */
function place($section)
{
    return View::place($section);
}

/**
 * A shortcut function for View::section
 * @param $sectionName
 */
function section($sectionName)
{
    View::section($sectionName);
}

/**
 * A shortcut function for View::sectionEnd
 */
function sectionEnd()
{
    View::sectionEnd();
}