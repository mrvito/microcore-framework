<?php

class DatabaseTest extends TestCase
{
    /** @test */
    function it_can_connect_to_a_database()
    {
        Database::connect();

        $this->asserttrue(Database::isConnected());
    }
}