<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Whoops...</title>
    <style type="text/css">
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 1024px;
            margin: auto;
            padding: 0 16px 0 16px;
        }

        .lead {
            font-size: 24px;
            line-height: 24px;
            color: #0085b1;
        }

        .head {
            background-color: #0085b1;
            padding: 4px 32px 4px 32px;
            margin-bottom: 32px;
            color: #fff;
            box-shadow: 0 0 16px rgba(0, 0, 0, 0.4);
        }
    </style>
</head>
<body>
<div class="head">
    <h1>Whoops...</h1>
</div>
<div class="container">
    <p class="lead">Something went wrong...</p>
</div>
</body>
</html>