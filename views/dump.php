<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Let's debug it!</title>
    <style type="text/css">
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        td {
            padding-left: 16px;
        }

        .container {
            max-width: 1024px;
            margin: auto;
            padding: 0 16px 0 16px;
        }

        .tip {
            line-height: 14px;
            font-size: 14px;
            color: #999;
            margin: 24px 0 4px 0;
        }

        .info {
            line-height: 14px;
            font-size: 14px;
            color: #777;
            margin: 4px 0 4px 0;
        }

        .lead {
            font-size: 24px;
            line-height: 24px;
            color: #009b67;
        }

        .small {
            font-size: 18px;
            line-height: 18px;
            color: #666;
        }

        hr.hr-style-b {
            height: 12px;
            border: 0;
            box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.2);
            margin: 24px 0 24px 0;
        }

        hr.hr-style-c {
            margin: 16px 0;
            border: 0;
            border-bottom: 1px solid #eaeaea;
        }

        .spaced {
            margin: 16px 0 16px 0;
        }

        .head {
            background-color: #009b67;
            padding: 4px 32px 4px 32px;
            margin-bottom: 32px;
            color: #fff;
            box-shadow: 0 0 16px rgba(0, 0, 0, 0.4);
        }

        .code {
            background-color: #f7f7f7;
            border-radius: 4px;
            border: 1px solid #f0f0f0;
            padding: 16px;
        }
    </style>
</head>
<body>
<div class="head">
    <h1>Let's debug it!</h1>
</div>
<div class="container">
    <p class="tip">Output (print_r):</p>
    <pre class="code"><?= $print_r ?></pre>
    <p class="tip">Output (var_dump):</p>
    <pre class="code"><?= $var_dump ?></pre>
</div>
<hr class="hr-style-b"/>
<div class="container">
    <p class="lead">Input data:</p>
    <p class="small spaced">$_GET</p>
    <?php if (empty($_GET)): ?>
        <p class="info">No variables...</p>
    <?php else: ?>
        <?php foreach ($_GET as $key => $value): ?>
            <p class="info"><?= $key ?> = <?= $value ?></p>
        <?php endforeach ?>
    <?php endif ?>
    <hr class="hr-style-c"/>
    <p class="small spaced">$_POST</p>
    <?php if (empty($_POST)): ?>
        <p class="info">No variables...</p>
    <?php else: ?>
        <?php foreach ($_POST as $key => $value): ?>
            <p class="info"><?= $key ?> =
            <?php if (is_array($value)): echo 'Array:' ?>
                <table>
                    <?php foreach ($value as $k => $v): ?>
                        <tr>
                            <td class="info"><?= $k ?></td>
                            <td class="info"><?= $v ?></td>
                        </tr>
                    <?php endforeach ?>
                </table>
            <?php else: ?>
                <?= $value ?>
            <?php endif ?>
            </p>
        <?php endforeach ?>
    <?php endif ?>
    <hr class="hr-style-c"/>
    <p class="small spaced">$_SERVER</p>
    <table>
        <?php foreach ($_SERVER as $key => $value): ?>
            <tr>
                <td class="info"><?= $key ?></td>
                <td class="info"><?= $value ?></td>
            </tr>
        <?php endforeach ?>
    </table>
    <br/>
</div>
<hr class="hr-style-b"/>
<div class="container">
    <p class="tip">Microcore v<?= VERSION ?></p>
    <br/>
</div>
</body>
</html>