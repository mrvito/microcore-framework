<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Houston, we have a problem</title>
    <style type="text/css">
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        td {
            padding-left: 16px;
        }

        .container {
            max-width: 1024px;
            margin: auto;
            padding: 0 16px 0 16px;
        }

        .text-center {
            text-align: center;
        }

        .tip {
            line-height: 14px;
            font-size: 14px;
            color: #999;
            margin: 24px 0 4px 0;
        }

        .info {
            line-height: 14px;
            font-size: 14px;
            color: #777;
            margin: 4px 0 4px 0;
        }

        .lead {
            font-size: 24px;
            line-height: 24px;
            color: #0085b1;
        }

        .small {
            font-size: 18px;
            line-height: 18px;
            color: #666;
        }

        .dim {
            color: #ccc;
        }

        hr.hr-style-a {
            border: 0;
            border-bottom: 1px solid #ccc;
        }

        hr.hr-style-b {
            height: 12px;
            border: 0;
            box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.2);
            margin: 24px 0 24px 0;
        }

        hr.hr-style-c {
            margin: 16px 0;
            border: 0;
            border-bottom: 1px solid #eaeaea;
        }

        .spaced {
            margin: 16px 0 16px 0;
        }

        .head {
            background-color: #0085b1;
            padding: 4px 32px 4px 32px;
            margin-bottom: 32px;
            color: #fff;
            box-shadow: 0 0 16px rgba(0, 0, 0, 0.4);
        }
    </style>
</head>
<body>
<div class="head">
    <h1>Houston, we have a problem</h1>
</div>
<div class="container">
    <p class="lead dim"><?= get_class($exception) ?></p>
    <p class="tip">Error message:</p>
    <p class="lead"><?= $exception->getMessage(); ?></p>
    <p class="tip">On:</p>
    <p class="small"><?= $exception->getFile(); ?></p>
    <p class="tip">At:</p>
    <p class="small">Line: <?= $exception->getLine(); ?></p>
</div>
<hr class="hr-style-b"/>
<div class="container">
    <p class="lead">Stack trace:</p>
    <br/>
    <?php foreach ($exception->getTrace() as $key => $trace): ?>
        <p class="info"><span class="dim"><?= $key ?>&ensp;</span>
            <?php if (isset($trace['file'])): ?>
                <span>File: <?= $trace['file'] ?> @ Line: <?= $trace['line'] ?></span>
            <?php else: ?>
                <span>&bullet;</span>
            <?php endif ?>
        </p>
        <p class="info">&ensp;&ensp;Thrown at: <?=
            isset($trace['class']) ? $trace['class'] : '',
            isset($trace['type']) ? $trace['type'] : '',
            $trace['function'] ?></p>
        <hr class="hr-style-a"/>
    <?php endforeach ?>
</div>
<hr class="hr-style-b"/>
<div class="container">
    <p class="lead">Input data:</p>
    <p class="small spaced">$_GET</p>
    <?php if (empty($_GET)): ?>
        <p class="info">No variables...</p>
    <?php else: ?>
        <?php foreach ($_GET as $key => $value): ?>
            <p class="info"><?= $key ?> = <?= $value ?></p>
        <?php endforeach ?>
    <?php endif ?>
    <hr class="hr-style-c"/>
    <p class="small spaced">$_POST</p>
    <?php if (empty($_POST)): ?>
        <p class="info">No variables...</p>
    <?php else: ?>
        <?php foreach ($_POST as $key => $value): ?>
            <p class="info"><?= $key ?> =
            <?php if (is_array($value)): echo 'Array:' ?>
                <table>
                    <?php foreach ($value as $k => $v): ?>
                        <tr>
                            <td class="info"><?= $k ?></td>
                            <td class="info"><?= $v ?></td>
                        </tr>
                    <?php endforeach ?>
                </table>
            <?php else: ?>
                <?= $value ?>
            <?php endif ?>
            </p>
        <?php endforeach ?>
    <?php endif ?>
    <hr class="hr-style-c"/>
    <p class="small spaced">$_SERVER</p>
    <table>
        <?php foreach ($_SERVER as $key => $value): ?>
            <tr>
                <td class="info"><?= $key ?></td>
                <td class="info"><?= is_array($value) ? '[' . join(', ', $value) . ']' : $value ?></td>
            </tr>
        <?php endforeach ?>
    </table>
    <br/>
</div>
<hr class="hr-style-b"/>
<div class="container">
    <p class="tip">Microcore v<?= VERSION ?></p>
    <br/>
</div>
</body>
</html>