<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Whoops...</title>
    <style type="text/css">
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 1024px;
            margin: auto;
            padding: 0 16px 0 16px;
        }

        .lead {
            font-size: 24px;
            line-height: 24px;
            color: #0085b1;
        }

        .massive {
            font-size: 92px;
            margin-bottom: 0;
            color: #EDEDED;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container text-center">
    <h1 class="massive"><?= $code ?></h1>
    <p class="lead"><?= $message ?></p>
</div>
</body>
</html>