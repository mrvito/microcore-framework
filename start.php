<?php

define('VERSION', '0.2.3');

define('FRAMEWORK_ROOT', __DIR__);

require('helpers.php');

$appOutput = '';

function errorHandler($errno, $errstr, $errfile, $errline)
{
    cleanOutputBuffer();
    exceptionHandler(new ErrorException($errstr, $errno, 1, $errfile, $errline));

    return true;
}

function dd($data)
{
    cleanOutputBuffer();

    $print_r = print_r($data, true);

    ob_start();
    var_dump($data);
    $var_dump = ob_get_clean();

    if(App::inCli()) {
        echo $print_r . PHP_EOL;
        exit();
    }

    if (conf('app.debug')) {
        include FRAMEWORK_ROOT . '/views/dump.php';
    } else {
        include Config::get('misc.whoops-view');
    }

    exit();
}

/**
 * @param Exception $exception
 */
function exceptionHandler($exception)
{
    cleanOutputBuffer();

    if(App::inCli()) {
        echo 'Message: ' . $exception->getMessage() . PHP_EOL;
        echo 'Code: ' . $exception->getCode() . PHP_EOL;
        echo 'File: ' . $exception->getFile() . PHP_EOL;
        echo 'Line: ' . $exception->getLine() . PHP_EOL;
        echo 'Stack trace: ' . PHP_EOL . $exception->getTraceAsString() . PHP_EOL;

        return;
    }

    if (conf('app.debug')) {
        include FRAMEWORK_ROOT . '/views/exception.php';
    } else {
        include Config::get('misc.whoops-view');
    }
}

function onShutdown()
{
    $error = error_get_last();

    if ($error !== null) {
        exceptionHandler(new ErrorException($error['message'], 0, 1, $error['file'], $error['line']));
    }

    flushOutputBuffer();
}

function cleanOutputBuffer()
{
    if(checkOutputBuffer()) {
        ob_clean();
    }
}

function flushOutputBuffer()
{
    if(checkOutputBuffer()) {
        ob_end_flush();
    }
}

function checkOutputBuffer()
{
    return ob_get_level() == 0 ? false : true;
}

register_shutdown_function('onShutdown');
set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');

App::run();
