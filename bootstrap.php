<?php
// First we define a root path...
define('ROOT', __DIR__ . '/../../..');
// We need to load things...
require(__DIR__ . '/../../autoload.php');
// And now lets have a fresh start
require(__DIR__ . '/start.php');