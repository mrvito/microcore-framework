<?php

class Database
{
    /**
     * @var PDO Database driver
     */
    private static $driver;

    /**
     * Connects to the MySQL database using information from the config file.
     * @param string $database Database to connect to.
     */
    public static function connect($database = null)
    {
        if ($database == null) {
            $database = conf('database.database');
        }

        self::connectDatabase($database);
    }

    /**
     * Executes given query on the currently connected database.
     * @param string $query The query to execute.
     * @return array Associative array with query results.
     */
    public static function execute($query)
    {
        $result = self::query($query);

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getLastInsertId()
    {
        return self::driver()->lastInsertId();
    }

    public static function lastError()
    {
        return self::driver()->errorCode();
    }

    public static function setVariable($name, $value)
    {
        self::execute("SET @$name = '$value'");
    }

    protected static function query($query)
    {
        return self::driver()->query($query);
    }

    /**
     * @param string $database
     * @throws \ErrorException
     */
    private static function connectDatabase($database)
    {
        $host = conf('database.host');
        $user = conf('database.user');
        $password = conf('database.password');

        self::$driver = new PDO("mysql:dbname={$database};host={$host};charset=utf8", $user, $password);
    }

    private static function driver()
    {
        if ( ! self::$driver) {
            throw new ErrorException('No database connection.');
        }

        return self::$driver;
    }

    public static function isConnected()
    {
        return (bool) self::driver();
    }
}
