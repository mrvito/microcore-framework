<?php

abstract class Model
{
    public static $table;

    /**
     * @var $query Query
     */
    protected $query;

    protected $fields;

    protected $fillable = [];

    protected $primaryKey = 'id';

    public function __construct($query)
    {
        $this->query = $query;
    }

    public static function create($fields)
    {
        $model = new static(null);
        $model->fillFields($fields, $model->fillable);
        return $model;
    }

    public function fill($fields)
    {
        $this->fillFields($fields, $this->fillable);
        return $this;
    }

    /**
     * @param mixed $key
     * @param null|string $column
     * @return Model
     */
    public static function one($key, $column = null)
    {
        $model = static::select();

        if (!$column) {
            $column = $model->primaryKey;
        }

        $collection = $model->where($column, $key)->get();

        return $collection->first();
    }

    public static function all()
    {
        $model = static::select();

        return $model->get();
    }

    public static function select($columns = null)
    {
        $query = Query::selectFrom(static::$table, $columns);
        $model = new static($query);

        return $model;
    }

    public function where($column, $searchString, $operator = '=')
    {
        $this->query->where($column, $searchString, $operator);
        return $this;
    }

    public function andWhere($column, $searchString, $operator = '=')
    {
        $this->query->andWhere($column, $searchString, $operator);
        return $this;
    }

    public function orWhere($column, $searchString, $operator = '=')
    {
        $this->query->orWhere($column, $searchString, $operator);
        return $this;
    }

    public function orderBy($columns, $direction = 'ASC')
    {
        $this->query->orderBy($columns, $direction);
        return $this;
    }

    public function limit($count, $offset = 0)
    {
        $this->query->limit($count, $offset);
        return $this;
    }

    /**
     * @return Collection
     */
    public function get()
    {
        $results = $this->query->get();

        $modelObjects = [];

        if ($results) {
            foreach ($results as $result) {
                $modelObjects[] = self::instantiate($result, $this->query);
            }
        } else {
            return null;
        }

        return new Collection($modelObjects);
    }

    public function save()
    {
        $primaryKey = $this->{$this->primaryKey};

        if ($primaryKey) {
            Query::updateOn(static::$table, $this->getFields($this->primaryKey))->where($this->primaryKey, $primaryKey)->get();
        } else {
            Query::insertInto(static::$table, $this->getFields($this->primaryKey));
            $primaryKey = Database::getLastInsertId();

            if (!$primaryKey == 0) {
                $this->{$this->primaryKey} = $primaryKey;
                $this->query = Query::selectFrom(static::$table)->where($this->primaryKey, $primaryKey);
            }
        }

        return $this;
    }

    public function delete()
    {
        Query::deleteFrom(static::$table)->where($this->primaryKey, $this->id)->get();
        return true;
    }

    protected final static function instantiate($model, $query)
    {
        $modelObject = new static($query);

        $modelObject->fillFields($model);

        return $modelObject;
    }

    protected final function fillFields($model, $only = null)
    {
        $fields = [];

        if (is_array($only)) {
            foreach ($model as $key => $value) {
                if (!in_array($key, $only)) {
                    continue;
                }

                $fields[$key] = $value;
            }

            $fields[$this->primaryKey] = $this->fields[$this->primaryKey];
        } else {
            $fields = $model;
        }

        $this->fields = $fields;
    }

    protected function getFields($except)
    {
        $fields = [];

        foreach ($this->fields as $key => $value) {
            if (in_array($key, $except)) {
                continue;
            }

            $fields[$key] = $value;
        }

        return $fields;
    }

    public function __get($name)
    {
        if (isset($this->fields[$name])) {
            return $this->fields[$name];
        }

        return null;
    }

    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }
}