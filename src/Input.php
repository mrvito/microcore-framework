<?php

class Input
{
    /**
     * @var array
     */
    protected static $data;


    public static function load()
    {
        $dataFromGet = $_GET;
        $dataFromPost = $_POST;

        self::$data = array_replace_recursive($dataFromGet, $dataFromPost);
    }

    public static function get($key)
    {
        if (self::exists($key)) {
            return self::$data[$key];
        } else {
            return null;
        }
    }

    /**
     * @return array
     */
    public static function all()
    {
        return self::$data;
    }

    public static function set($key, $value)
    {
        self::$data[$key] = $value;
    }

    public static function exists($key)
    {
        return isset(self::$data[$key]);
    }

    public static function find($search, $returnValue = false)
    {
        return self::search($search, array_keys(self::$data), $returnValue);
    }

    public static function findByValue($search, $returnValue = false)
    {
        return self::search($search, array_values(self::$data), $returnValue);
    }

    private static function search($search, $data, $returnValue)
    {
        $matches = preg_grep('/.*' . $search . '.*/', $data);
        if (!empty($matches)) {
            if ($returnValue) {
                return array_shift(array_values($matches));
            } else {
                return array_shift(array_keys($matches));
            }
        }

        return null;
    }

    public static function check($key, $value)
    {
        return self::get($key) === $value;
    }

    public static function checkAndSet($key, $value, $newKey, $newValue, $setOnFalse = false)
    {
        $result = self::check($key, $value);

        if ($result) {
            self::set($newKey, $newValue);
            return true;
        } elseif ($setOnFalse) {
            self::set($newKey, $newValue);
            return true;
        } else {
            return false;
        }
    }

    public static function checkAndReplace($key, $value, $newValue, $replaceOnFalse = false)
    {
        return self::checkAndSet($key, $value, $key, $newValue, $replaceOnFalse);
    }

    public static function map($keys)
    {
        $newData = array();

        foreach (self::$data as $data) {
            if (count($keys) === 0) {
                break;
            }

            $newData[array_shift($keys)] = $data;
        }

        self::$data = $newData;
    }

    public static function toJSON()
    {
        return json_encode(self::$data);
    }
}