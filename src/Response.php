<?php

class Response
{
    protected $code;
    protected $data;
    protected static $headers = [];

    public function __construct($code = 200, $data = null)
    {
        $this->code = $code;
        $this->data = $data;
    }

    public static function json($data, $code = 200)
    {
        $data = json_encode($data);

        $response = new static($code, $data);
        $response->addHeader('Content-Type: application/json');

        return self::handle($response);
    }

    public static function redirect($url)
    {
        $response = new static(302);
        $response->addHeader('Location: /' . $url);

        return self::handle($response);
    }

    public static function back()
    {
        return self::redirect($_SESSION['previous_request']);
    }

    public static function view(View $view)
    {
        $response = new static(200, $view);

        return self::handle($response);
    }

    protected static function handle(Response $response)
    {
        if (App::isRouting()) {
            return $response;
        } else {
            $response->flush();
            return true;
        }
    }

    public static function addHeader($header)
    {
        self::$headers[] = $header;
    }

    public function flush()
    {
        http_response_code($this->code);

        foreach (self::$headers as $header) {
            header($header);
        }

        if ($this->data instanceof View) {
            $this->data->render();
        } else {
            echo $this->data;
        }
    }
}