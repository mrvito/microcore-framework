<?php

class View
{
    protected $view;
    protected $data;

    protected static $shared = [];

    /**
     * @var static|null
     */
    protected static $currentView = null;

    /**
     * @var static|null
     */
    protected $nextView = null;

    protected $extending = null;
    protected $openSection = null;
    protected $sections = [];

    protected function __construct($view, $data = [])
    {
        $this->view = $view;
        $this->data = $data;
    }

    public static function make($view, $data = [])
    {
        if (App::isRouting()) {
            return self::makeStatic($view, $data);
        } else {
            return self::makeRender($view, $data);
        }
    }

    public static function makeFromFile($viewFile, $data = [])
    {
        if (!file_exists($viewFile)) {
            throw new ErrorException('View file does not exist: ' . $viewFile);
        }

        return new static($viewFile, $data);
    }

    public static function inject($view, $data = [])
    {
        self::make($view, $data)->render();
    }

    public static function share($key, $value)
    {
        self::$shared[$key] = $value;
    }

    protected static function makeStatic($view, $data = [])
    {
        $viewPath = Config::get('paths.views') . str_replace('.', '/', $view) . '.php';
        $viewFullPath = ROOT . '/' . $viewPath;
        if (!file_exists($viewFullPath)) {
            throw new ErrorException('View file does not exist: ' . $viewPath);
        }

        return new static($viewFullPath, $data);
    }

    protected static function makeRender($view, $data = [])
    {
        return self::makeStatic($view, $data)->render();
    }

    public function getSection($section)
    {
        if (isset($this->sections[$section])) {
            return $this->sections[$section];
        }

        return null;
    }

    public static function place($section)
    {
        return static::$currentView->getSection($section);
    }

    public static function extend($layout)
    {
        static::$currentView->nextView = static::make($layout, static::$currentView->data);
    }

    public static function section($sectionName)
    {
        static::$currentView->nextView->openSection($sectionName);
    }

    protected function openSection($sectionName)
    {
        $this->openSection = $sectionName;

        ob_start();
    }

    public static function sectionEnd()
    {
        static::$currentView->nextView->closeSection();
    }

    protected function closeSection()
    {
        if ($this->openSection) {
            $this->sections[$this->openSection] = ob_get_clean();
        }

        $this->openSection = null;
    }

    public function render()
    {
        foreach (self::$shared as $key => $value) {
            $$key = $value;
        }

        foreach ($this->data as $key => $value) {
            $$key = $value;
        }

        static::$currentView = $this;

        include $this->view;

        if ($this->nextView) {
            $this->nextView->render();
        }

        return true;
    }
}