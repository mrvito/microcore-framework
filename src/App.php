<?php

class App
{
    protected static $routing = false;

    protected static $instance;

    public function __construct()
    {
        if(static::inCli()) {
            $this->runCli();

            return;
        }

        $this->runWeb();
    }

    public static function run()
    {
        static::$instance = new App();
    }

    public static function inCli()
    {
        return php_sapi_name() == 'cli';
    }

    public function runCli()
    {
        session_start();

        Config::load();
    }

    public function runWeb()
    {
        ob_start();
        session_start();

        Config::load();
        Input::load();
        Request::resolve();

        if (conf('database.connect')) {
            Database::connect();
        }

        if (file_exists(ROOT . '/routes.php')) {
            self::$routing = true;

            require ROOT . '/routes.php';

            $result = Router::resolve();
            self::finish($result);
        }
    }

    public static function abort($statusCode, $statusMessage = null)
    {
        $visibleMessage = '';
        $jsonMessage = '';

        if (!$statusMessage) {
            switch ($statusCode) {
                case 404: {
                    $visibleMessage = 'Page not found';
                    $jsonMessage = '404 Not Found';
                    break;
                }
                default: {
                    $visibleMessage = 'Unknown error';
                    $jsonMessage = 'Unknown error';
                }
            }
        }

        if (Request::isAjax()) {
            $result = Response::json(['error' => $jsonMessage], $statusCode);
        } else {
            $result = View::makeFromFile(Config::get('misc.error-view'), ['message' => $visibleMessage, 'code' => $statusCode]);
        }

        self::finish($result);

        exit();
    }

    public static function finish($result)
    {
        if ($result instanceof Response) {
            $result->flush();
        } else {
            $output = new Response(200, $result);
            $output->flush();
        }
    }

    public static function isRouting()
    {
        return self::$routing;
    }
}