<?php

class Assets
{
    protected static $usedScripts = [];
    protected static $usedStyles = [];

    protected static $scripts;
    protected static $styles;

    /**
     * @param string $scriptName Name of the script that was registered using registerScript method
     */
    public static function useScript($scriptName)
    {
        if (isset(self::$scripts[$scriptName])) {
            self::$usedScripts[] = self::$scripts[$scriptName];
        }
    }

    /**
     * @param string $styleName Name of the style that was registered using registerScript method
     */
    public static function useStyle($styleName)
    {
        if (isset(self::$styles[$styleName])) {
            self::$usedStyles[] = self::$styles[$styleName];
        }
    }

    public static function registerScript($scriptName, $pathToScript, $version = '1.0', $absoluteUrl = false)
    {
        self::$scripts[$scriptName] = $absoluteUrl ? $pathToScript : asset($pathToScript) . '?v=' . $version;
    }

    public static function registerStyle($styleName, $pathToStyle, $version = '1.0', $absoluteUrl = false)
    {
        self::$styles[$styleName] = $absoluteUrl ? $pathToStyle : asset($pathToStyle) . '?v=' . $version;
    }

    public static function registerAndUseScript($scriptName, $pathToScript, $version = '1.0', $absoluteUrl = false)
    {
        self::registerScript($scriptName, $pathToScript, $version, $absoluteUrl);
        self::useScript($scriptName);
    }

    public static function registerAndUseStyle($styleName, $pathToStyle, $version = '1.0', $absoluteUrl = false)
    {
        self::registerStyle($styleName, $pathToStyle, $version, $absoluteUrl);
        self::useStyle($styleName);
    }

    public static function insertScripts()
    {
        $output = '';

        foreach (self::$usedScripts as $script) {
            $output .= '<script src="' . $script . '"></script>';
        }

        return $output;
    }

    public static function insertStyles()
    {
        $output = '';

        foreach (self::$usedStyles as $style) {
            $output .= '<link rel="stylesheet" href="' . $style . '"/>';
        }

        return $output;
    }
}

