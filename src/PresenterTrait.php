<?php

trait PresenterTrait
{
    private $retrieving = false;
    private $presenters = [];

    public function __construct($query)
    {
        parent::__construct($query);

        $this->presenters = $this->present();
    }

    public function __get($name)
    {
        if ($this->retrieving) {
            return parent::__get($name);
        } else {
            if (isset($this->presenters[$name])) {
                $this->retrieving = true;
                $result = $this->presenters[$name](clone $this);
                $this->retrieving = false;

                return $result;
            } else {
                return parent::__get($name);
            }
        }
    }

    public function registerPresenters(array $presenters)
    {
        $this->presenters = $presenters;
    }

    /**
     * Return an associative array with key => value
     * where key is the new property name to present
     * and value is a closure that returns a value for the new property
     * @return array
     */
    abstract public function present();
}