<?php

class WhereClause
{
    private $clauses;

    public function __construct($column, $searchString, $operator = "=")
    {
        $this->add($column, $searchString, $operator);
    }

    private function add($column, $search, $operator, $joint = null)
    {
        $this->clauses[] =
            [
                'column' => $column,
                "operator" => $operator,
                "search" => $search,
                "joint" => $joint
            ];
    }

    public function andWhere($column, $searchString, $operator)
    {
        $this->add($column, $searchString, $operator, "AND");
    }

    public function orWhere($column, $searchString, $operator)
    {
        $this->add($column, $searchString, $operator, "OR");
    }

    public function __toString()
    {
        $whereString = "WHERE ";

        foreach ($this->clauses as $clause) {
            $whereString .= ($clause['joint'] ? " " . $clause['joint'] . " " : "") . "`" . $clause['column'] . "` " . $clause['operator'] . " '" . $clause['search'] . "'";
        }

        return $whereString;
    }
}