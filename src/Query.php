<?php

class Query
{
    private $action;
    private $table;
    private $columns;
    private $fields;
    private $executable = '';

    private $distinct = false;
    private $limit = false;
    private $offset = 0;
    private $orderBy = false;
    private $orderByColumns;

    /**
     * @var $whereClause WhereClause
     */
    private $whereClause;

    /**
     * @var $joinClause JoinClause
     */
    private $joinClause;

    private function __construct($action, $table)
    {
        $this->action = $action;
        $this->table = $table;
    }

    /**
     * @param $columns
     * @param $table
     * @return Query
     */
    public static function selectFrom($table, $columns = null)
    {
        $query = new Query("select", $table);

        $_columns = [];

        if (!$columns) {
            $_columns = null;
        } elseif (is_string($columns)) {
            $_columns[] = $columns;
        } else {
            $_columns = $columns;
        }

        $query->columns = $_columns;

        return $query;
    }

    public static function selectDistinct($table, $columns = null)
    {
        $query = self::selectFrom($table, $columns);

        $query->distinct = true;

        return $query;
    }

    public static function countFrom($table)
    {
        $query = new Query("count", $table);

        return $query;
    }

    public static function deleteFrom($table)
    {
        $query = new Query('delete', $table);

        return $query;
    }

    public static function insertInto($table, $fields)
    {
        $query = new Query('insert', $table);

        $query->fields = $fields;

        return $query->get();
    }

    public static function updateOn($table, $fields)
    {
        $query = new Query('update', $table);

        $query->fields = $fields;

        return $query;
    }

    /**
     * @param $column
     * @param $searchString
     * @param $operator
     * @return Query
     */
    public function where($column, $searchString, $operator = "=")
    {
        $this->whereClause = new WhereClause($column, $searchString, $operator);

        return $this;
    }

    /**
     * @param $column
     * @param $searchString
     * @param $operator
     * @return Query
     */
    public function andWhere($column, $searchString, $operator = "=")
    {
        if (!$this->whereClause) {
            return $this;
        }

        $this->whereClause->andWhere($column, $searchString, $operator);

        return $this;
    }

    /**
     * @param $column
     * @param $searchString
     * @param $operator
     * @return Query
     */
    public function orWhere($column, $searchString, $operator = "=")
    {
        if (!$this->whereClause) {
            return $this;
        }

        $this->whereClause->orWhere($column, $searchString, $operator);

        return $this;
    }

    public function leftJoin($secondTable, $key, $secondKey)
    {
        if (!$this->joinClause) {
            $this->joinClause = new JoinClause();
        }

        $this->joinClause->leftJoin($this->table, $secondTable, $key, $secondKey);

        return $this;
    }

    public function rightJoin($secondTable, $key, $secondKey)
    {
        if (!$this->joinClause) {
            $this->joinClause = new JoinClause();
        }

        $this->joinClause->rightJoin($this->table, $secondTable, $key, $secondKey);

        return $this;
    }

    public function innerJoin($secondTable, $key, $secondKey)
    {
        if (!$this->joinClause) {
            $this->joinClause = new JoinClause();
        }

        $this->joinClause->innerJoin($this->table, $secondTable, $key, $secondKey);

        return $this;
    }

    public function orderBy($columns, $direction = 'ASC')
    {
        $this->orderBy = $direction;

        $_columns = [];

        if (is_string($columns)) {
            $_columns[] = $columns;
        } else {
            $_columns = $columns;
        }

        $this->orderByColumns = $_columns;

        return $this;
    }

    public function limit($count, $offset = 0)
    {
        $this->limit = $count;
        $this->offset = $offset;

        return $this;
    }

    public function get()
    {
        $this->buildQuery();

        $result = Database::execute($this->executable);

        return $this->prepareResult($result);
    }

    private function prepareResult($result)
    {
        switch ($this->action) {
            case "count": {
                return $result[0]["COUNT(id)"];
            }
            default: {
                return $result;
            }
        }
    }

    private function buildQuery()
    {
        $q = [];
        $before = '';
        $after = ';';

        switch ($this->action) {
            case "select": {
                $q[] = 'SELECT';
                $q[] = $this->distinct ? 'DISTINCT' : false;
                $q[] = $this->formatColumns();
                $q[] = 'FROM';
                $q[] = $this->formatTable();
                $q[] = $this->joinClause;
                $q[] = $this->whereClause;

                if ($this->orderBy) {
                    $q[] = 'ORDER BY';
                    $q[] = $this->formatOrderByColumns();
                    $q[] = $this->orderBy;
                }

                if ($this->limit) {
                    $q[] = 'LIMIT';
                    $q[] = $this->limit;
                    if ($this->offset > 0) {
                        $q[] = 'OFFSET';
                        $q[] = $this->offset;
                    }
                }

                break;
            }
            case "count": {
                $q[] = 'SELECT COUNT(id) FROM';
                $q[] = $this->formatTable();
                $q[] = $this->whereClause;

                break;
            }
            case 'insert': {
                $q[] = 'INSERT INTO';
                $q[] = $this->formatTable();
                $q[] = $this->formatInsertFields();

                break;
            }
            case 'update': {
                $q[] = 'UPDATE';
                $q[] = $this->formatTable();
                $q[] = $this->joinClause;
                $q[] = 'SET';
                $q[] = $this->formatUpdateFields();
                $q[] = $this->whereClause;

                break;
            }
            case 'delete': {
                $q[] = 'DELETE FROM';
                $q[] = $this->formatTable();
                $q[] = $this->whereClause;

                break;
            }
        }

        $this->executable = $before . join(' ', array_filter($q)) . $after;
    }

    private function formatTable()
    {
        return '`' . $this->table . '`';
    }

    private function formatColumns()
    {
        $columns = $this->columns;

        if (!$columns) {
            return "*";
        }

        array_walk($columns, function (&$value) {
            $value = "`" . $value . "`";
        });

        return join(", ", $columns);
    }

    private function formatOrderByColumns()
    {
        $orderByColumns = $this->orderByColumns;

        array_walk($orderByColumns, function (&$value) {
            $value = "`" . $value . "`";
        });

        return join(", ", $orderByColumns);
    }

    private function formatUpdateFields()
    {
        $updateFields = array();

        foreach ($this->fields as $key => $value) {
            $updateFields[] = '`' . addslashes($key) . '`=\'' . addslashes($value) . '\'';
        }

        return join(', ', $updateFields);
    }

    private function formatInsertFields()
    {
        $keys = [];
        $values = [];

        foreach ($this->fields as $key => $item) {
            $keys[] = '`' . addslashes($key) . '`';
            $values[] = "'" . addslashes($item) . "'";
        }

        $keysString = join(',', $keys);
        $valuesString = join(',', $values);

        return '(' . $keysString . ') VALUES (' . $valuesString . ')';
    }
}