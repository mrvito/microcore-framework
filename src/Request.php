<?php

class Request
{
    const PROTOCOL_HTTPS = 'HTTPS';
    const PROTOCOL_HTTP = 'HTTP';

    protected static $requestUri;

    public static function resolve()
    {
        self::resolveRequestUri();
    }

    private static function resolveRequestUri()
    {
        if ( ! isset($_SERVER['REQUEST_URI'])) {
            self::$requestUri = [];
            return;
        }

        $requestUri = $_SERVER['REQUEST_URI'];

        $e_requestUri = array_values(array_filter(explode('?', $requestUri)));

        if ( ! count($e_requestUri)) {
            self::$requestUri = [];
            return;
        }

        $e_requestUri = array_values(array_filter(explode('/', $e_requestUri[0])));

        self::$requestUri = $e_requestUri;
    }

    public static function getRequestUri()
    {
        if (empty(self::$requestUri)) {
            return '/';
        } else {
            return join('/', self::$requestUri);
        }
    }

    public static function getRequestMethod()
    {
        if ( ! isset($_SERVER['REQUEST_METHOD'])) {
            return null;
        }

        $method = $_SERVER['REQUEST_METHOD'];

        if ($method == 'GET') {
            return $method;
        } else {
            if (isset($_POST['__method'])) {
                if ($_POST['__method'] == 'PUT') {
                    return 'PUT';
                } elseif ($_POST['__method'] == 'DELETE') {
                    return 'DELETE';
                } else {
                    return $method;
                }
            } else {
                return $method;
            }
        }
    }

    public static function getDomainUri()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public static function getProtocol()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)
            ? static::PROTOCOL_HTTPS : static::PROTOCOL_HTTP;
    }

    public static function isAjax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}