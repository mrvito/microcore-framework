<?php

class Config
{
    protected static $config;

    public static function load()
    {
        self::$config = include(FRAMEWORK_ROOT . '/config.php');

        if (file_exists(ROOT . '/config.php')) {
            self::$config = array_replace_recursive(self::$config, include(ROOT . '/config.php'));
        }
    }

    /**
     * @param string $path Dot notated path to a configuration item.
     * @return mixed|null
     */
    public static function get($path)
    {
        return array_dot(self::$config, $path);
    }

    public static function getAll()
    {
        return self::$config;
    }
}