<?php

class Router
{
    /**
     * @var Route[]
     */
    protected static $routes = [];

    /**
     * @var Route
     */
    protected static $current;

    public static function resolve()
    {
        self::resolveRequest();

        if (!self::$current) {
            App::abort(404);
        }

        self::resolveMethod();
        self::resolveParameters();

        if (self::$current->passGuard()) {
            self::$current->handle();
        }

        if (self::$current->getMethod() == 'GET') {
            self::storeRequest();
        }

        return self::$current->getResponse();
    }

    private static function storeRequest()
    {
        $_SESSION['previous_request'] = isset($_SESSION['current_request']) ? $_SESSION['current_request'] : '/';
        $_SESSION['current_request'] = $_SERVER['REQUEST_URI'];
    }

    protected static function resolveMethod()
    {
        if (Request::getRequestMethod() != self::$current->getMethod()) {
            throw new ErrorException('HTTP request method is not allowed');
        }
    }

    protected static function resolveRequest()
    {
        $requestUri = Request::getRequestUri();

        $matchingRoutes = array_values(array_filter(self::getMethodRoutes(Request::getRequestMethod()), function ($route) use ($requestUri) {
            return preg_match(self::resolveRouteExpression($route), $requestUri);
        }));

        if (empty($matchingRoutes)) {
            self::$current = null;
        } else {
            self::$current = array_shift($matchingRoutes);
        }
    }

    protected static function resolveParameters()
    {
        $requestUri = Request::getRequestUri();

        $params = [];

        preg_match(self::resolveRouteExpression(self::$current), $requestUri, $params);
        array_shift($params);

        $names = self::$current->getParameterNames();

        $parameters = array_filter(array_combine($names, $params));

        self::$current->setParameters($parameters);
    }

    protected static function resolveRouteExpression(Route $route)
    {
        $path = str_replace('/', '\/', $route->getPath());
        $expression = preg_replace(["/{(\w+\?)}/", "/{(\w+)}/"], ["?([_\-A-Za-z0-9]*)", "([_\-A-Za-z0-9]+)"], $path);
        return "/^" . $expression . "$/";
    }

    public static function addRoute(Route $route)
    {
        self::$routes[] = $route;
    }

    public static function getAllRoutes()
    {
        return self::$routes;
    }

    public static function getMethodRoutes($method)
    {
        return array_filter(self::$routes, function ($route) use ($method) {
            /**
             * @var $route Route
             */
            return $route->getMethod() == $method;
        });
    }

    public static function getCurrent()
    {
        return self::$current;
    }
}