<?php

class JoinClause
{
    private $clauses;

    public function __construct()
    {
        $this->clauses = [];
    }

    private function add($joinType, $table, $secondTable, $key, $secondKey)
    {
        $this->clauses[] =
            [
                'join' => $joinType,
                'table' => $table,
                'second_table' => $secondTable,
                "key" => $key,
                "second_key" => $secondKey
            ];
    }

    public function leftJoin($table, $secondTable, $key, $secondKey)
    {
        $this->add('LEFT JOIN', $table, $secondTable, $key, $secondKey);
    }

    public function rightJoin($table, $secondTable, $key, $secondKey)
    {
        $this->add('RIGHT JOIN', $table, $secondTable, $key, $secondKey);
    }

    public function innerJoin($table, $secondTable, $key, $secondKey)
    {
        $this->add('INNER JOIN', $table, $secondTable, $key, $secondKey);
    }

    public function __toString()
    {
        $joins = [];

        foreach ($this->clauses as $clause) {
            $joins[] = $clause['join'] . ' `' . $clause['second_table'] . '` ON `' . $clause['table'] . '`.`' . $clause['key'] . '`=`' . $clause['second_table'] . '`.`' . $clause['second_key'] . '`';
        }

        return join(' ', $joins);
    }
}