<?php

class Route
{
    protected $method;
    protected $path;
    protected $handlerClass;
    protected $handlerFunction;
    protected $name;
    protected $response;
    protected $parameters;
    protected $controller;
    protected $guard = null;

    protected static $guarded = false;

    protected function __construct($method, $path, $handler, $name = null)
    {
        if (is_string($handler)) {
            $e_handler = explode('@', $handler);

            if (count($e_handler) >= 2) {
                $this->handlerClass = $e_handler[1];
                $this->handlerFunction = $e_handler[0];
            } else {
                throw new ErrorException('Invalid route handler definition: ' . $handler);
            }
        } elseif (is_callable($handler)) {
            $this->handlerClass = null;
            $this->handlerFunction = $handler;
        } else {
            throw new ErrorException('Invalid route handler definition: ' . $handler);
        }

        $this->method = $method;
        $this->path = $path;
        $this->name = $name;
        $this->response = null;

        if (self::$guarded) {
            $this->guard = self::$guarded;
        }
    }

    public static function get($path, $handler, $name = null)
    {
        Router::addRoute(new static('GET', $path, $handler, $name));
    }

    public static function post($path, $handler, $name = null)
    {
        Router::addRoute(new static('POST', $path, $handler, $name));
    }

    public static function put($path, $handler, $name = null)
    {
        Router::addRoute(new static('PUT', $path, $handler, $name));
    }

    public static function delete($path, $handler, $name = null)
    {
        Router::addRoute(new static('DELETE', $path, $handler, $name));
    }

    public static function guard($guardName, Closure $callback)
    {
        self::$guarded = $guardName;

        $callback();

        self::$guarded = false;
    }

    public function passGuard()
    {
        if (!$this->guard) {
            return true;
        }

        if (!file_exists(ROOT . '/guards.php')) {
            throw new ErrorException('To use route guards you must have guards.php file at your root directory');
        }

        require_once ROOT . '/guards.php';

        if (function_exists($this->guard)) {
            $result = call_user_func($this->guard);

            if ($result !== true) {
                $this->response = $result;
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function handle()
    {
        if ($this->handlerClass == null) {
            $this->response = call_user_func_array($this->handlerFunction, $this->parameters);
        } else {
            $class = new ReflectionClass('App\\Controllers\\' . $this->handlerClass);
            $this->controller = $class->newInstance();
            $method = new ReflectionMethod($this->controller, $this->handlerFunction);
            $this->response = $method->invokeArgs($this->controller, $this->parameters);
        }
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getParameterNames()
    {
        $matches = [];
        preg_match_all("/{(\w+\??)}/", $this->path, $matches);

        if (empty($matches[1])) {
            return [];
        }

        return $matches[1];
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getController()
    {
        return $this->controller;
    }
}