<?php

class File
{
    protected $name;
    protected $type;
    protected $tmp;
    protected $error;
    protected $size;

    public function getType()
    {
        return $this->type;
    }

    protected function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return File[]|File|null
     */
    public static function all($name = 'files')
    {
        $files = [];

        if (isset($_FILES[$name])) {
            if (is_array($_FILES[$name]['name'])) {
                foreach ($_FILES[$name]['name'] as $key => $value) {
                    if ($_FILES[$name]['error'][$key]) {
                        continue;
                    }

                    $file = new static($value);
                    $file->type = $_FILES[$name]['type'][$key];
                    $file->tmp = $_FILES[$name]['tmp_name'][$key];
                    $file->size = $_FILES[$name]['size'][$key];

                    $files[] = $file;
                }
            } else {
                if ($_FILES[$name]['error']) {
                    return null;
                }

                $file = new static($_FILES[$name]['name']);
                $file->type = $_FILES[$name]['type'];
                $file->tmp = $_FILES[$name]['tmp_name'];
                $file->size = $_FILES[$name]['size'];

                return $file;
            }

            if (empty($files)) {
                return null;
            }

            return $files;
        } else {
            return null;
        }
    }

    public function move($path)
    {
        $fullPath = ROOT . '/' . $path;

        if (!$this->error) {
            $moved = move_uploaded_file($this->tmp, $fullPath);

            if ($moved) {
                return $fullPath;
            }
        }

        return false;
    }
}